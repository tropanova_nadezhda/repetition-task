package ru.tns.task11;

import java.util.Scanner;

/**
 * Программа, которая считывает количество часов, минут и секунд в n-ном количестве суток
 *
 * @author Tropanova N.S.
 */
public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество дней: ");
        int days = scanner.nextInt();
        convertTime(days);
    }

    private static void convertTime(int days) {
        int hour = 24 * days;
        int min = 60 * hour;
        int sec = 60 * min;

        System.out.println(hour + " часов " + min + " минут " + sec + " секунд");
    }
}