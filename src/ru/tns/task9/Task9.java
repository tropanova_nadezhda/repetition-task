package ru.tns.task9;

import java.util.Scanner;
/**
 * Класс для проверки целых чисел на палиндром
 *
 * @autor Tropanova N.S.
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число : ");
        String s = scanner.nextLine();
        if (s.equals(new StringBuffer(s).reverse().toString())) {
            System.out.println("Палиндром! :)");
        } else {
            System.out.println("Не палиндром! :(");
        }

    }
}
// reverse() — меняет порядок всех символов на противоположный
// equals () - используется для сравнения равенства двух объектов

