package ru.tns.task4;

/*
 * Расчет расстояние до места удара молнии.
 *
 * @autor Tropanova N.S.
 */
public class Lightning {
    public static void main(String[] args) {
        final double interval = 6.8; // в секундах
        final double speedOfSound = 1234.8; // в км/ч
        double distance = (speedOfSound / 3600) * interval;
        System.out.println(distance);
    }
}

