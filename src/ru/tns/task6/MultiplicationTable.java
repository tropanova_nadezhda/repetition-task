package ru.tns.task6;

import java.util.Scanner;

/**
 * Класс для реализации таблицы умножения
 *
 * @author Tropanova N.S.
 */
public class MultiplicationTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int i = scanner.nextInt();
        mulTable(i);
    }
    /**
     * Метод умножает введённое число и вывод
     *
     * @param i число которое надо перемножать
     */
    private static void mulTable(int i) {
        for (int j = 1; j <= 10; j++) {
            System.out.println(i + " x " + j + " = " + (i * j));
        }
    }
}
