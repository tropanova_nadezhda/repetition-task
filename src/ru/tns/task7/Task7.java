package ru.tns.task7;

import java.util.Scanner;

/**
 * Программа для проверки является ли число типа double целым.
 *
 * @author Tropanova N.S.
 */
public class Task7 {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        double number = scanner.nextDouble();
        if (number % 1 == 0) {
            System.out.println("Это целое число");
        } else {
            System.out.println("Это дробное число");
        }
    }
}

