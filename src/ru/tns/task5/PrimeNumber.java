package ru.tns.task5;

/**
 * Класс для реализации вывода простых чисел от 2 до 100
 */
public class PrimeNumber {
    public static void main(String[] args) {
        for (int i = 2; i <= 100; i++) {  // цикл перебирает числа от 2 до 100
            int temp = 0;
            for (int j = 2; j < i; j++) { //цикл перебирает числа от двух до текущего числа
                if ((i % j) == 0)
                    temp++;
            }
            if (temp < 1)
                System.out.println(i + " простое число");
        }
    }
}
