package ru.tns.task3;

import java.util.Scanner;

/**
 * Класс для перевода рублей в евро по заданному курсу
 *
 * @author Tropanova N.S.
 */
public class Money {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите курс евро: ");
        double euro = scanner.nextDouble();
        System.out.println("Введите сумму в рублях: ");
        double ruble = scanner.nextDouble();
        System.out.println("Вы можете купить " + conversionOfRublesToEuros(euro, ruble) + " евро.");
    }

    /**
     * Метод для передова рублей в евро по заданному курсу
     *
     * @param euro  курс евро
     * @param ruble рубли
     * @return сколько можно купить евро по заданному курсу
     */
    private static double conversionOfRublesToEuros(double euro, double ruble) {
        return ruble / euro;

    }
}
