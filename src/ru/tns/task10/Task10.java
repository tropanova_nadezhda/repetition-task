package ru.tns.task10;

import java.util.Scanner;

/**
 * Класс для проверки строки на палиндром
 *
 * @autor Tropanova N.S.
 */
public class Task10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите фразу : ");
        String s = scanner.nextLine();
        isPalindrome(s);
    }

    /**
     * Метод проверки фразы на палиндромность
     *
     * @param s фраза для проверки
     */
    private static void isPalindrome(String s) {
        s = s.replaceAll("[\\d+\\s()?:!.,;{}\"-]", ""); //удаляем все ненужное
        if (s.toLowerCase().equals((new StringBuffer(s)).reverse().toString().toLowerCase())) { //присваиваем перевернутую строку

            System.out.println("Палиндром! :)");
        } else {
            System.out.println("Не палиндром! :(");
        }
    }
}


// toLowerCase() - Преобразует все буквы в строке к нижнему регистру
// reverse() - меняет порядок всех символов на противоположный
// equals () - используется для сравнения равенства двух объектов
// toString() - используется для получения строкового объекта

