package ru.tns.task1;
/*
 * Cчитывает символы пока не встретится точка, а также  вывод кол-ва пробелов.
 *
 * @autor Tropanova N.S.
 */
import java.util.Scanner;

public class Task1 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите строку:");
        String string = scanner.nextLine();

        if (!string.contains(".")) {
            System.out.println("Нет точки");
            return;
        }

        //substring() возвращаемое значение заданной подстрокой.
        String lengthStr = string.substring(0, string.indexOf('.'));

        // indexOf() позволяет возвратить индекс искомого элемента в массиве при первом совпадении
        int lengthString = string.indexOf("."); // длина строки

        // replaceAll() — заменяет каждую подстроку данной строки
        lengthStr = lengthStr.replaceAll(" ", ""); // длина строки до точки

        int whitespace = lengthString - lengthStr.length();

        System.out.println("Количество символов до точки: " + lengthString);
        System.out.println("Количество пробелов: " + whitespace);
    }
}

