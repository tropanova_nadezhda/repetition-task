package ru.tns.task2;
/*
 * Напишите метод, который будет увеличивать заданный элемент массива на 10%.
 *
 * @autor Tropanova N.S.
 */

import java.util.Scanner;

public class Percent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] i = {10, 110, 210, 310, 510, 20, 50, 70, 60, 90};

        System.out.println("Введите номер элемента: ");
        int j = scanner.nextInt() - 1;

        System.out.println(i[j] + " + 10% = " + increaseOfElements(i,j));
    }

    /*
     * Увеличивает заданный элемент массива на 10%
     *
     * @param i массив
     * @param j индекс заданного эл-та
     * @return значение заданного элемента, увеличенного на 10%
     */
    private static double increaseOfElements(int[] i, int j) {
        return i[j] += i[j] * 0.1;

    }
}
